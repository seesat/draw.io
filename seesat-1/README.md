# Draw.io

[diagrams.net](https://www.diagrams.net)/[draw.io](https://draw.io) is an open source technology stack for building diagramming applications, and the world’s most widely used browser-based end-user diagramming software.

This repository is the secure online storage of all diagrams related to SeeSat. If you have read permissions on it, you can connect it to **draw.io** and display the stored diagrams (it is possible to view all revisions/versions). If you have write permissions, you may even edit and change them or add completely new diagrams - but please don't make a mess ;-)
